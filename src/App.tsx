import React from 'react'
import './App.css'

type Switch2Props = {
  onChange?: (checked: boolean) => void
}
class Switch2 extends React.Component<Switch2Props> {
  input!: HTMLInputElement

  get value() {
    return this.input.checked
  }

  set value(checked: boolean) {
    this.input.checked = checked
    this.props.onChange?.(checked)
  }

  toggle = () => {
    this.value = !this.value
  }

  render() {
    return (
      <>
        <input
          type="checkbox"
          ref={e => (this.input = e!)}
          onChange={() => this.props.onChange?.(this.value)}
        />
        <button onClick={this.toggle}>toggle</button>
        <button onClick={() => console.log(this.value)}>test</button>
      </>
    )
  }
}

class Switch extends React.Component {
  inputRef = React.createRef<HTMLInputElement>()

  toggle() {
    let input = this.inputRef.current!
    input.checked = !input.checked
  }

  getValue() {
    return this.inputRef.current?.checked
  }

  render() {
    return (
      <>
        <input type="checkbox" ref={this.inputRef} />
        <button
          onClick={() => {
            console.log('test')
            console.log('value:', this.getValue())
          }}
        >
          test
        </button>
        <button onClick={() => this.toggle()}>toggle</button>
      </>
    )
  }
}

const App: React.FC = () => {
  const [switchElement, setSwitchElement] = React.useState<Switch2 | null>(null)
  function on() {
    if (switchElement) {
      switchElement.value = true
    }
  }
  function off() {
    if (switchElement) {
      switchElement.value = false
    }
  }
  return (
    <div className="App">
      <Switch2
        onChange={checked => {
          console.log({ checked })
        }}
        ref={setSwitchElement}
      />
      <hr />
      <button onClick={on}>On</button>
      <button onClick={off}>Off</button>
      <button onClick={switchElement?.toggle}>Toggle</button>
    </div>
  )
}

export default App
